{{ csrf_field()}}
<x-form.input
    type="text"
    description="Código do cupom"
    name="code"
    value="{{ $data['code'] ?? null  }}"
    errorMessage="{{ $errors->first('code') }}"
/>
<x-buttons.form-action
    routeBack="{{ route('home') }}"
    descriptionAction="Resgatar"
    iconAction="fa fa-check"
    submit="true"
/>


