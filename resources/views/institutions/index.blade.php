<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <div class="row">

            @forelse($data as $institution)
                <div class="col-md-4 mt-3">
                    <div class="card h-100">
                        <img src="{{ $institution->coverImage }}" class="img elevation-2" style="height: auto; width: auto; max-width:100%; max-height:200px" alt="Capa da instituição">
                        <div class="card-body">
                            <h5 class="card-title">{{ $institution->name }}</h5>
                            <p class="card-text">{!! Str::of($institution->description)->before('<br />')->limit(500, '...') !!}</p>
                        </div>
                        <div class="card-footer clearfix">
                            <a href="{{ route('institutions.show', $institution->id) }}" class="btn btn-info float-right">Mais detalhes</a>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-md-12 text-center">
                    <p>Sem instituições cadastradas no momento.</p>
                </div>
            @endforelse
        </div>
        <div class="row mt-3">
            <div class="col-md-12 text-center">
                {!! $data->links('vendor.pagination.bootstrap-4') !!}
            </div>
        </div>
    </x-admin.internal-card>
    @push('scripts')
        @toastr_render
    @endpush
</x-admin>

