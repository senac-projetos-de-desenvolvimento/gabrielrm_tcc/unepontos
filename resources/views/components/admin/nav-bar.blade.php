<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>

    <ul class="navbar-nav ml-auto">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a class="nav-link" href="{{route('profile.edit')}}">
                    <i class="fas fa-user-astronaut"></i>
                </a>
            </li>
            <li class="nav-item">
                <button type="submit" form="logout-form" class=" btn nav-link">
                    <i class="fas fa-power-off"></i>
                </button>
                <form id="logout-form"
                      action="{{ route('logout') }}"
                      method="POST" class="d-none">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>

    </ul>
</nav>
