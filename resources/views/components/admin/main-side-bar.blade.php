<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ asset('images/up_logo.png') }}"
             alt="{{ config('app.name', 'UnePontos') }} Logo"
             class="brand-image">
        <span class="brand-text font-weight-light">{{ config('app.name', 'UnePontos') }}</span>
    </a>
    <x-admin.side-bar/>
</aside>
