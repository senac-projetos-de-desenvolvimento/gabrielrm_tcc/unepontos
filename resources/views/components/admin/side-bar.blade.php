<div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <a href="{{route('profile.edit')}}">
                <img src="{{ $userProfileImage }}" class="img-circle elevation-2" alt="Imagem de perfil do usuário">
            </a>
        </div>
        <div class="info">
            <a href="{{route('profile.edit')}}" class="d-block">{{ $userName }}</a>
        </div>
    </div>

    <x-admin.side-bar-menu />
</div>
