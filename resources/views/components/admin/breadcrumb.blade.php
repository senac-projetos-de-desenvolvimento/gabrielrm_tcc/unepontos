<ol class="breadcrumb float-sm-right">
    @foreach($breadcrumbs as $breadcrumb)
        <li class="breadcrumb-item {{ $breadcrumb['url'] ?? 'active' }}">
            @if($breadcrumb['url'])
                <a href="{{ $breadcrumb['url'] }}">{{ $breadcrumb['title'] }}</a>
            @else
                {{ $breadcrumb['title'] }}
            @endif
        </li>
    @endforeach
</ol>
