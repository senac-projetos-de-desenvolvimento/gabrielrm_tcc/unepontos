<footer class="main-footer">
    <strong>© {{date('Y')}} <a href="{{ route('home') }}" target="_blank">{{ config('app.name') }}</a>
</footer>
