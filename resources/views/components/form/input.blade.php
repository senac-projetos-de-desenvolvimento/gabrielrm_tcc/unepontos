<div class="form-group row @if($errorMessage) has-danger @endif">
    <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    <input type="{{ $type }}" value="{{ old($name) ?? $value }}" name="{{ $name }}" class="form-control @if($errorMessage) is-invalid @endif">
    @if($errorMessage)
        <div class="invalid-feedback">{{ $errorMessage }}</div>
    @endif
</div>
