<div class="form-group row @if($errorMessage) has-danger @endif" contenteditable="true">
    <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    @php
        $value = str_replace( '<br />', "", $value);
    @endphp
    <textarea class="form-control @if($errorMessage) is-invalid @endif" rows="10" name="{{ $name }}">{!! old($name) ?? $value !!}</textarea>
    @if($errorMessage)
        <div class="invalid-feedback">{{ $errorMessage }}</div>
    @endif
</div>
