<div class="form-group row">
    <label for="{{ $name }}" class="col-form-label">{{ $description }}</label>
    <input type="{{ $type }}" value="{{ $value }}" name="{{ $name }}" class="form-control" disabled>
</div>
