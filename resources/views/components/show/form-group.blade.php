<div class="col-12">
    <div class="form-group row mb-0">
        <label class="control-label col-md-3 col-lg-2 text-bold">{{ $description }}</label>
        <div class="col-md-9 cl-lg-10">
            {{ $slot }}
        </div>
    </div>
</div>
