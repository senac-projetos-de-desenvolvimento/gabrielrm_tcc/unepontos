<x-admin title="{{ $page['titlePage'] }}" :breadcrumbs="$page['breadcrumb']">
    <x-admin.internal-card>
        <div class="row row-cols-1 row-cols-md-2">
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-body">
                        <a href="{{ route('institutions.show', $data['institution']->id) }}" class="btn btn-primary btn-lg"
                           data-toggle="tooltip" data-placement="top" data-original-title="Solicitar Resgate" title="Solicitar Resgate">
                            <i class="fas fa-eye"></i> Ver {{ $data['institution']->name }}
                        </a>
                        <a href="{{ route('institutions.edit', $data['institution']->id) }}" class="btn btn-info btn-lg"
                           data-toggle="tooltip" data-placement="top" data-original-title="Solicitar Resgate" title="Solicitar Resgate">
                            <i class="fa fa-edit"></i> Editar {{ $data['institution']->name }}
                        </a>
                        <ul class="list-group mt-4">
                            <li class="list-group-item active d-flex justify-content-between align-items-center">
                                Últimas doações
                            </li>
                            @forelse($data['lastDonations'] as $donation)
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    {{ $donation->user->name }}
                                    <span class="badge badge-primary badge-pill">{{ $donation->points }}</span>
                                </li>
                            @empty
                                <li class="list-group-item d-flex justify-content-between align-items-center">
                                    Essa instituição ainda não recebeu doações.
                                </li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-100">
                    <div class="card-header">
                        <h3 class="card-title">Pontos recebidos</h3>
                    </div>
                    <div class="card-body text-center">
                        @if($data['availablePoints'] > 0)
                            <h1>Você possui o total de <span class="badge badge-info">{{ $data['availablePoints'] }}</span> pontos para resgate.</h1>
                            <a href="{{ route('rescues.create') }}" class="btn btn-info btn-lg"
                               data-toggle="tooltip" data-placement="top" data-original-title="Solicitar Resgate" title="Solicitar Resgate">
                                <i class="fas fa-donate"></i> Solicitar Resgate
                            </a>
                        @else
                            <h2>Opa, você ainda não tem pontos disponíveis.</h2>
                            <h5>Ajude divulgando UnePontos e sua instituição nas redes sociais.</h5>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <canvas id="chartPoints" style="width:100%; max-height: 500px;"></canvas>
            </div>
        </div>
    </x-admin.internal-card>
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js" integrity="sha512-Wt1bJGtlnMtGP0dqNFH1xlkLBNpEodaiQ8ZN5JLA5wpc1sUlk/O5uuOMNgvzddzkpvZ9GLyYNa8w2s7rqiTk5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        @toastr_render
        <script>
            $(function () {
                var donations =  {!! json_encode($data['donationsByMonth']) !!};
                var xValues = donations.label;
                var yValues = donations.data;
                var sumPoints = yValues.reduce((acumulador, numero) => {
                    return acumulador += numero;
                }, 0)

                const CHART_COLORS = {
                    green: 'rgb(75, 192, 192)',
                    blue: 'rgb(54, 162, 235)',
                    purple: 'rgb(153, 102, 255)',
                    yellow: 'rgb(255, 205, 86)',
                    red: 'rgb(255, 99, 132)',
                    orange: 'rgb(255, 159, 64)',
                    grey: 'rgb(201, 203, 207)'
                };

                new Chart("chartPoints", {
                    type: "doughnut",
                    data: {
                        labels: xValues,
                        datasets: [
                            {
                                label: 'Pontos',
                                data: yValues,
                                backgroundColor: Object.values(CHART_COLORS),
                            }
                        ]
                    },
                    options: {
                        responsive: true,
                        plugins: {
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: `Pontos recebidos nos últimos 6 meses: ${sumPoints} `
                            }
                        }
                    },
                });
            });
        </script>
    @endpush
</x-admin>

