{{ csrf_field()}}
<div class="form-group row @if($errors->first('code')) has-danger @endif">
    <label for="code" class="col-form-label">Código do cupom</label>
    <div class="input-group input-group-sm">
        <input type="text" value="{{ old('code') ??  $data['code'] ?? null }}" name="code" class="form-control @if($errors->first('code')) is-invalid @endif">
        <span class="input-group-append">
            <button type="submit" name="save" class="btn btn-info btn-flat">Resgatar</button>
          </span>
        @if($errors->first('code'))
            <div class="invalid-feedback">{{ $errors->first('code') }}</div>
        @endif
    </div>

</div>
