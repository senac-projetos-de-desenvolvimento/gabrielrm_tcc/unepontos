@extends('layouts.admin.app')
@section('bread')
    <div class="col-sm-6">
        <ol class="breadcrumb float-sm-left">
            <li class="breadcrumb-item"><a href="{{route('home')}}">Inicio</a></li>
            <li class="breadcrumb-item"><a href="{{route($page['route'].'index')}}">{{$page['titlePage']}}</a></li>
            <li class="breadcrumb-item active">Visualizar</li>
        </ol>
    </div>
@stop

@section('content')
    <div class="card-body">
        <form class="form-horizontal" role="form">
            <div class="row">
                <div class="col-12">
                    <div class="form-group row mb-0">
                        <label class="control-label col-md-3 col-lg-2 text-bold">Título:</label>
                        <div class="col-md-9 cl-lg-10">
                            <p class="form-control-static"> {{$data->title}} </p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row mb-0">
                        <label class="control-label col-md-3 col-lg-2 text-bold">Ícone:</label>
                        <div class="col-md-9 cl-lg-10">
                            <p class="form-control-static"> <i class="{{$data->icon}} "></i></p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row mb-0">
                        <label class="control-label col-md-3 col-lg-2 text-bold">URL:</label>
                        <div class="col-md-9 cl-lg-10">
                            <p class="form-control-static">{{$data->slug}}</p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group row mb-0">
                        <label class="control-label col-md-3 col-lg-2 text-bold">Superior:</label>
                        <div class="col-md-9 cl-lg-10">
                            <p class="form-control-static">{{$data->parent->title ?? ''}}</p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-12">
                    <div class="form-group row mb-0">
                        <label class="control-label col-md-3 col-lg-2 text-bold">Data Criação:</label>
                        <div class="col-md-9 col-lg-10">
                            <p class="form-control-static"> {{ date('d/m/Y',strtotime($data->created_at))}} </p>
                        </div>
                    </div>
                </div>
                <!--/span-->
                <div class="col-12">
                    <p><strong class="mr-1">Status:</strong>
                        @if($data->active=='S')
                            <span class="text-success">Ativo</span>
                        @else
                            <span class="text-danger">Inativo</span>
                        @endif
                    </p>
                </div>
            </div>
            <hr>
            <div class="form-actions float-left">
                <a href="{{ route($page['route'].'index')}}" class="btn btn-light"><i
                        class="fas fa-long-arrow-alt-left"></i> Voltar</a>
            </div>
            <div class="form-actions float-right">
                <a href="{{ route($page['route'].'edit',$data->id) }}" class="btn btn-warning"><i class="fa fa-edit"></i> Editar</a>
            </div>
        </form>
    </div>
@section('js')
@stop
@stop


