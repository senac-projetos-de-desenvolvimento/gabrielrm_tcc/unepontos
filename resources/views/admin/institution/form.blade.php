{{ csrf_field()}}

@role('admin')
    <x-form.input
        type="text"
        description="Nome"
        name="name"
        value="{{ $data['name'] ?? null  }}"
        errorMessage="{{ $errors->first('name') }}"
    />
    <x-form.input
        type="text"
        description="CNPJ"
        name="cnpj"
        value="{{ $data['cnpj'] ?? null  }}"
        errorMessage="{{ $errors->first('cnpj') }}"
    />
@else
    <x-form.input-disabled
        type="text"
        description="Nome"
        name="name"
        value="{{ $data['name'] ?? null }}"
    />
    <x-form.input-disabled
        type="text"
        description="CNPJ"
        name="cnpj"
        value="{{ $data['cnpj'] ?? null }}"
    />
@endrole

<x-form.input-file
    description="Imagem de Capa"
    name="file"
    value=""
    errorMessage="{{ $errors->first('file') }}"
/>
<x-form.text-area
    type="text"
    description="Descrição"
    name="description"
    value="{!! isset($data['description']) ? $data['description'] : null  !!}"
    errorMessage="{{ $errors->first('description') }}"
/>


<x-buttons.form-action
    routeBack="{{ route($page['route'].'index') }}"
    descriptionAction="Salvar"
    iconAction="fa fa-check"
    submit="true"
/>


