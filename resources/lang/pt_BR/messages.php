<?php
return [
    'successfully_created' => 'Criado com sucesso',
    'api_healthy' => 'Api is healthy',
    'insufficient_funds' => 'Saldo insuficiente',
    'insufficient_outstanding_balance' => 'Saldo pendente unsificiente!',
    'error_when_withdrawing' => 'Erro ao realizar o saque, aguarde e tente novamente mais tarde!',
    'withdrawal_in_processing' => 'Saque em processamento, aguarde!',
    'try_again_later' => 'Erro, tente novamente mais tarde',
    'unauthorized' => 'Não autorizado',
    'successfully_logged_out' => 'Deslogado com sucesso',
    'user_in_not_logged_in' => 'Usuário não logado',
    'unauthorized_type' => 'Tipo não autorizado',
    'error_when_canceling' => 'Erro ao cancelar',
    'unauthorized_user' => 'Usuário não autorizado',
    'successfully_deleted' => 'Excluído com sucesso!',
    'error_deleting' => 'Erro ao excluir!',
    'updated_successfully' => 'Atualizado com sucesso!',

];
