require('./bootstrap');
require('datatables.net-bs4/js/dataTables.bootstrap4');
require('datatables.net-buttons/js/buttons.html5.js');
require('datatables.net-buttons-bs4');
require('datatables.net-buttons/js/buttons.print.js');

require( 'datatables.net-responsive-bs4' );
require( 'datatables.net-responsive' );

import $ from "jquery";
window.datatables_translate = {
    "sEmptyTable": "Nenhum registro encontrado",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
    "sInfoPostFix": "",
    "sInfoThousands": ".",
    "sLengthMenu": "_MENU_ resultados por página",
    "sLoadingRecords": "Carregando...",
    "sProcessing": "Processando...",
    "sZeroRecords": "Nenhum registro encontrado",
    "sSearch": "",
    "sSearchPlaceholder": " Pesquisar",
    "oPaginate": {
        "sNext": "Próximo",
        "sPrevious": "Anterior",
        "sFirst": "Primeiro",
        "sLast": "Último"
    },
    "oAria": {
        "sSortAscending": ": Ordenar colunas de forma ascendente",
        "sSortDescending": ": Ordenar colunas de forma descendente"
    }
};
var col= new Array();
$(document).ready(function () {

    $('table.d-table:first > thead > tr > th').each(function(i) {
        col.push(i);
    });col.pop();

    $('table.d-table:first , table.d-table:nth-child(1)').DataTable({
        dom: 'Bfrtip',
        buttons: [{
            extend: 'copy',
            exportOptions: {columns: col},
            text: '<i class="fas fa-copy" data-toggle="tooltip" data-placement="top" title="Copiar texto"></i>'
        }, {
            extend: 'csv',
            exportOptions: {columns: col},
            text: '<i class="fas fa-file-csv" data-toggle="tooltip" data-placement="top" title="Gerar CSV"></i>'
        }, {
            extend: 'print', exportOptions: {columns: col},
            customize: function ( win ) {
                $(win.document.body)
                    .css( 'font-size', '10pt' )
                // .prepend(
                //     '<img src="http://localhost:8001/images/jus-pqn.png" style="position:absolute; top:0; left:0;" />'
                // );

                $(win.document.body).find( 'table' )
                    .addClass( 'compact' )
                    .css( 'font-size', 'inherit' );
            },
            text: '<i class="fas fa-print" data-toggle="tooltip" data-placement="top" title="Imprimir"></i>'
        },],
        //"dom": '<"pull-left"f><"pull-right"l>tip',
        'paging': true,
        //'lengthChange': true,
        'searching': true,
        'ordering': true,
        'lengthChange': false,
        'info': true,
        language: datatables_translate,
        "lengthMenu": [[12, 24, -1], [12, 24, "All"]]
    })

});
