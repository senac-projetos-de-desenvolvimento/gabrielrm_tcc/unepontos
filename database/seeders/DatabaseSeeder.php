<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //Admin
        $this->call(Admin\CreateRoleTableSeeder::class);
        $this->call(Admin\CreatePermissionAdminTableSeeder::class);
        $this->call(Admin\CreateModelHasPermissionTableSeeder::class);
        $this->call(Admin\CreateMenuAdminTableSeeder::class);
        $this->call(Admin\CreateMenuCompanyTableSeeder::class);
        $this->call(Admin\CreateMenuDonorTableSeeder::class);
        $this->call(Admin\CreateMenuInstitutionTableSeeder::class);



        //default
        $this->call(CityTableSeeder::class);
        $this->call(CreateInstitutionTableSeeder::class);
        $this->call(Admin\CreateUsersTableSeeder::class);
        //$this->call(CreatePermissionTableSeeder::class);

    }
}
