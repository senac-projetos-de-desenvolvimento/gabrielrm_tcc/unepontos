<?php
namespace Database\Seeders\Admin;
use App\Traits\SeederInsertTrait;
use Illuminate\Database\Seeder;

class CreateMenuDonorTableSeeder extends Seeder
{
    use SeederInsertTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = [
            [
                'title' => 'Painel de Controle',
                'icons' => 'fas fa-tachometer-alt',
                'url' => '/',
                'parent_id' => 0,
                'order' => 1,
            ],
            [
                'title' => 'Resgatar Cupom',
                'icons' => 'fas fa-ticket-alt',
                'url' => '/rescued-coupon',
                'parent_id' => 0,
                'order' => 2,
            ],
            [
                'title' => 'Doar',
                'icons' => 'fas fa-hand-holding-heart',
                'url' => '/donate',
                'parent_id' => 0,
                'order' => 3,
            ],
            [
                'title' => 'Instituições',
                'icons' => 'fas fa-dove',
                'url' => '/institutions',
                'parent_id' => 0,
                'order' => 4,
            ],
            [
                'title' => 'Empresas',
                'icons' => 'fas fa-store-alt',
                'url' => '/companies',
                'parent_id' => 0,
                'order' => 5,
            ],
        ];

        $this->insertMenuAndMenuRole($menus, 4);
    }
}
