<?php

namespace Tests\Feature;

use App\Models\Company;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminCompanyTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     */
    public function guest_cannot_see_company_listing()
    {
        $response = $this->get(route('companies.index'));

        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * @test
     */
    public function admin_show_list_companies()
    {
        $this->createAdminAndAuth();

        $response = $this->get(route('companies.index'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_view_create_page_companies()
    {
        $this->createAdminAndAuth();

        $response = $this->get(route('companies.create'));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_create_company_success()
    {
        $this->createAdminAndAuth();

        $data = $this->getDataSuccess();

        $response = $this->post(route('companies.store'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('companies.index'));
        $this->assertDatabaseHas('companies', $data);
    }

    /**
     * @test
     */
    public function admin_create_company_error()
    {
        $this->createAdminAndAuth();

        $data = $this->getDataError();

        $response = $this->json('post', route('companies.store'), $data);
        $response->assertStatus(422);
    }

    /**
     * @test
     */
    public function admin_view_edit_page_companies()
    {
        $this->createAdminAndAuth();

        $company = Company::factory()->create();

        $response = $this->get(route('companies.edit', $company->id));

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function admin_edit_company_success()
    {
        $this->createAdminAndAuth();

        $company = Company::factory()->create();

        $data =  $this->getDataSuccess();

        $response = $this->json('put', route('companies.update', $company->id), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('companies.index'));
        $this->assertDatabaseHas('companies', $data);
    }

    /**
     * @test
     */
    public function admin_edit_company_error()
    {
        $this->createAdminAndAuth();

        $company = Company::factory()->create();

        $data = $this->getDataError();

        $response = $this->json('put', route('companies.update', $company->id), $data);
        $response->assertStatus(422);
    }

    private function createAdminAndAuth()
    {
        $user = User::factory()->create();
        $user->assignRole('admin');

        $this->actingAs($user);
    }

    private function getDataSuccess()
    {
        return [
                'fantasy' => 'Nome fantasia',
                'company_name' => 'Minha Razao social',
                'cnpj' => '96325874123654',
                'description' => 'Description',
            ];
    }

    private function getDataError()
    {
        return [
            'fantasy' => 'Nome',
            'company_name' => 'Minha Razao social',
            'cnpj' => '234',
            'description' => 'Description',
        ];
    }
}
