<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserAttachRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if (empty($this->company_id)) {
            $this->request->remove('company_id');
        }

        if (empty($this->institution_id)) {
            $this->request->remove('institution_id');
        }
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'institution_id' => 'required_without_all:company_id|integer|exists:institutions,id',
            'company_id' => 'required_without_all:institution_id|integer|exists:companies,id',
        ];
    }
}
