<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\CouponRequest;
use App\Models\Coupon;
use App\Repositories\Contracts\CompanyRepositoryInterface;
use App\Repositories\Contracts\CouponRepositoryInterface;
use App\Traits\BreadcrumbTrait;
use Illuminate\Support\Facades\Auth;

class CouponController extends Controller
{
    use BreadcrumbTrait;
    private $couponRepository;
    private $companyRepository;

    const COUPON_RELATIONS = ['user', 'company', 'rescuedBy'];

    private string $route = "coupons.";
    private string $viewDir = 'coupon.';
    private string $titlePage = 'Cupons';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(CouponRepositoryInterface $couponRepository, CompanyRepositoryInterface $companyRepository)
    {
        $this->middleware('auth');

        $this->couponRepository = $couponRepository;
        $this->companyRepository = $companyRepository;

        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];

    }

    /**
     * Show the application admin.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if (! $user->can('coupon-list')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = [];

        if ($user->hasRole('admin')) {
            $data = $this->couponRepository->getWithRelations(self::COUPON_RELATIONS);
        }

        if ($user->hasRole('partner') && isset($user->company->id)) {
            $data = $this->couponRepository->getByAttributeWithRelations('company_id', $user->company->id, self::COUPON_RELATIONS);
        }

        return view($this->viewDir.'index')->with(['page' => $this->page, 'data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $user = Auth::user();
        if (! $user->can('coupon-create')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = [];

        if ($user->hasRole('admin')) {
            $data['companies'] = $this->companyRepository->getCompaniesPluckIdAndName();
        }

        if (! $user->hasRole('admin') && ! isset($user->company->id)) {
            toastr()->error('Usuário não vinculado a uma empresa');
            return redirect()->back();
        }

        if ($user->hasRole('partner') && isset($user->company->id)) {
            $data['companies'] = $this->companyRepository->getByAttribute('user_id', $user->id)->with('user')->get()->pluck('fantasy', 'id');
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');
        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CouponRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CouponRequest $request)
    {
        $user = Auth::user();
        if (! $user->can('coupon-create')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();

        if (! $user->hasRole('partner') && ! $user->hasRole('admin')) {
            toastr()->error('Usuário não vinculado aos papéis');
            return redirect()->back();
        }

        if (! $user->hasRole('admin') && isset($user->company->id) && $user->company->id != $data['company_id']) {
            toastr()->error('Usuário não pode gerar cupons');
            return redirect()->back();
        }

        $data['user_id'] = $user->id;
        $data['code'] = $data['company_id'].uniqid();
        $coupon = $this->couponRepository->create($data);

        toastr()->success(__('messages.successfully_created'));
        return redirect()->route($this->route.'show', [$coupon->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        if (! $user->can('coupon-list')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $this->couponRepository->findWithRelations($id, self::COUPON_RELATIONS);

        if (empty($data)) {
            toastr()->warning('Cupom não encontrado');
            return redirect()->back();
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Visualizar');
        return view($this->viewDir.'show')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if (! $user->can('coupon-edit')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $this->couponRepository->findWithRelations($id, self::COUPON_RELATIONS);

        if (empty($data)) {
            toastr()->warning('Cupom não encontrado');
            return redirect()->back();
        }

        if ($data->rescued == true) {
            toastr()->warning('Cupom já resgatado');
            return redirect()->back();
        }

        if ($user->hasRole('admin')) {
            $data['companies'] = $this->companyRepository->getCompaniesPluckIdAndName();
        }

        if ($user->hasRole('partner') && isset($user->company->id)) {
            $data['companies'] = $this->companyRepository->getByAttribute('user_id', $user->id)->with('user')->get()->pluck('fantasy', 'id');
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Editar');
        return view($this->viewDir.'edit')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CompanyUpdateRequest $request
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(CouponRequest $request, Coupon $coupon)
    {
        $user = Auth::user();
        if (! $user->can('coupon-edit')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();

        if (! $user->hasRole('partner') && ! $user->hasRole('admin')) {
            toastr()->warning('Usuário não vinculado aos papéis');
            return redirect()->back();
        }

        if (! $user->hasRole('admin') && isset($user->company->id) && $user->company->id != $data['company_id']) {
            toastr()->error('Usuário não pode editar cupons');
            return redirect()->back();
        }

        if ($coupon->rescued) {
            toastr()->warning('Cupom já resgatado');
            return redirect()->back();
        }

        $data['user_id'] = $user->id;
        $this->couponRepository->update($coupon->id, $data);

        toastr()->success('Atualizado com sucesso!');
        return redirect()->route($this->route.'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if (! $user->can('coupon-delete')) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        if (! $user->hasRole('partner') && ! $user->hasRole('admin')) {
            toastr()->warning('Usuário não vinculado aos papéis');
            return redirect()->back();
        }

        $coupon = $this->couponRepository->find($id);
        if (! empty($coupon) && $coupon->rescued == true) {
            toastr()->warning('Cupom já resgatado');
            return redirect()->back();
        }

        if (! $user->hasRole('admin') && isset($user->company->id) && $user->company->id != $coupon->id) {
            toastr()->error('Usuário não pode deletar cupons');
            return redirect()->back();
        }

        if  ($this->couponRepository->delete($id)) {
            toastr()->warning('Excluído com sucesso!');
            return redirect()->route($this->route.'index');
        }

        toastr()->error('Erro ao excluir!');
        return redirect()->route($this->route.'index');

    }
}
