<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserAttachRequest;
use App\Models\User;
use App\Repositories\Contracts\CompanyRepositoryInterface;
use App\Repositories\Contracts\InstitutionRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;

class UserAttachOrganizationUpdateController extends Controller
{
    private UserRepositoryInterface $userRepository;
    private CompanyRepositoryInterface $companyRepository;
    private InstitutionRepositoryInterface $institutionRepository;

    private string $route = "home.";
    private string $viewDir = 'admin.users.attach.';
    private string $titlePage = 'Atribuir usuário - Empresa/ONG';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        UserRepositoryInterface $userRepository,
        CompanyRepositoryInterface $companyRepository,
        InstitutionRepositoryInterface $institutionRepository,
    )
    {
        $this->middleware('auth');
        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];

        $this->middleware('permission:company-list');
        $this->middleware('permission:company-create', ['only' => ['create','store']]);
        $this->middleware('permission:company-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:institution-list');
        $this->middleware('permission:institution-create', ['only' => ['create','store']]);
        $this->middleware('permission:institution-edit', ['only' => ['edit','update']]);

        $this->userRepository = $userRepository;
        $this->companyRepository = $companyRepository;
        $this->institutionRepository = $institutionRepository;

    }

    public function __invoke(UserAttachRequest $request, User $user)
    {
        if (empty($user)) {
            toastr()->warning('Usuário não encontrado');
            return redirect()->back();
        }

        if (! $user->hasRole('partner') && ! $user->hasRole('institution')) {
            toastr()->error('Usuário não vinculado aos papéis');
            return redirect()->back();
        }

        $data = $request->validated();
        if ($user->hasRole('partner') && ! empty($data['company_id'])) {
           $this->companyRepository->update($data['company_id'], ['user_id' => $user->id]);
           toastr()->success('Usuário atribuído a empresa com sucesso!');
        }

        if ($user->hasRole('institution') && ! empty($data['institution_id'])) {
           $this->institutionRepository->update($data['institution_id'], ['user_id' => $user->id]);
           toastr()->success('Usuário atribuído a instituição com sucesso!');
        }
        return redirect()->route('users.index');
    }

}
