<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Requests\Admin\UserUpdateRequest;
use App\Repositories\Contracts\RoleRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\BreadcrumbTrait;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Arr;
use Response;
use Hash;

class UserController extends Controller
{
    use BreadcrumbTrait;
    private UserRepositoryInterface $userRepository;
    private RoleRepositoryInterface $roleRepository;

    private string $route = "users.";
    private string $viewDir = 'admin.users.';
    private string $titlePage = 'Usuários';

    public array $page = [];

    /**
     * DepartmentController constructor.
     * @param UserRepositoryInterface $userRepository
     * @param RoleRepositoryInterface $roleRepository
     */
    public function __construct(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository) {

        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;

        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir
        ];

        $this->middleware('permission:user-list');
        $this->middleware('permission:user-create', ['only' => ['create','store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->userRepository->getAllIdDescWithRelations(['media', 'company', 'institution']);
        $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->titlePage);

        return view($this->viewDir.'index')->with([
            'page'=>$this->page, 'data'=>$data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $data['all_roles'] = $this->roleRepository->getAllByName()->toArray();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');
        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);

        $output = $this->userRepository->create($data);
        $this->userRepository->syncRole($output->id,$data['role_id']);

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $this->userRepository->addImageProfile($output->id, $request->file('file'));
        }

        toastr()->success(__('messages.successfully_created'));
        return redirect()->route($this->route.'index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->userRepository->find($id);
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Visualizar');
        return view($this->viewDir.'show')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->userRepository->find($id);

        $data['all_roles'] = $this->roleRepository->getAllByName();

        $selected = false;
        foreach ($data['all_roles'] as $key => $obj) {

            foreach($data->roles as $r){
                if($r->id == $obj->id) {
                    $selected = true;
                    break;
                }
            }
            if($selected){
                $data['all_roles'][$key]->selected = true;
            }else{
                $data['all_roles'][$key]->selected = false;
            }
            $selected = false;
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Editar');
        return view($this->viewDir.'edit')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserUpdateRequest $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        $data = $request->all();
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            $data = Arr::except($data,array('password'));
        }

        $this->userRepository->update($user->id,$data);
        $this->userRepository->syncRole($user->id, $data['role_id']);

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $this->userRepository->addImageProfile($user->id, $request->file('file'));
        }

        toastr()->success('Atualizado com sucesso!');
        return redirect()->route($this->route.'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if  ($this->userRepository->delete($id)) {
            toastr()->warning('Excluído com sucesso!');
            return redirect()->route($this->route.'index');
        }

        toastr()->error('Erro ao excluir!');
        return redirect()->route($this->route.'index');

    }
}
