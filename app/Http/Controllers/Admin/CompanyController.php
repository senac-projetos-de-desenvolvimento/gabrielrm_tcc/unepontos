<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompanyRequest;
use App\Http\Requests\Admin\CompanyUpdateRequest;
use App\Models\Company;
use App\Repositories\Contracts\CompanyRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;
use App\Repositories\Contracts\WalletRepositoryInterface;
use App\Traits\BreadcrumbTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Response;

class CompanyController extends Controller
{
    use BreadcrumbTrait;
    private CompanyRepositoryInterface $companyRepository;
    private RoleRepositoryInterface $roleRepository;
    private WalletRepositoryInterface $walletRepository;

    private string $route = "companies.";
    private string $viewDir = 'admin.company.';
    private string $titlePage = 'Empresas';

    public array $page = [];

    public function __construct(
        CompanyRepositoryInterface $companyRepository,
        RoleRepositoryInterface $roleRepository,
        WalletRepositoryInterface $walletRepository,
    ) {
        $this->companyRepository = $companyRepository;
        $this->roleRepository = $roleRepository;
        $this->walletRepository = $walletRepository;

        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir
        ];

        $this->middleware('permission:company-list');
        $this->middleware('permission:company-create', ['only' => ['create','store']]);
        $this->middleware('permission:company-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:company-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        if ($user->hasRole(['admin'])) {
            $data = $this->companyRepository->getAll();
            $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->titlePage);

            return view($this->viewDir.'index')->with([
                'page'=>$this->page, 'data'=>$data
            ]);
        }

        $data = $this->companyRepository->paginate(6);
        $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->titlePage);

        return view('companies.index')->with([
            'page'=>$this->page, 'data'=>$data
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Criar');
        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CompanyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();
        $company = $this->companyRepository->create($data);

        if ($request->hasFile('file')) {
            $this->companyRepository->addCoverImage($company->id, $request->file('file'));
        }

        toastr()->success(__('messages.successfully_created'));
        return redirect()->route($this->route.'index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();

        $data['company'] = $this->companyRepository->find($id);
        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Visualizar');
        if ($user->hasRole(['admin'])) {
            return view($this->viewDir . 'show')
                ->with([
                    'page' => $this->page,
                    'data' => $data
                ]);
        }

        $data['availablePoints'] = $this->walletRepository->sumAvailableBalance($user->id);
        return view('companies.show')
            ->with([
                'page' => $this->page,
                'data' => $data
            ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin', 'partner'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $this->companyRepository->find($id);
        if (! $user->hasRole(['admin']) && $user->id != $data->user_id ) {
            toastr()->error('Usuário não vinculado');
            return redirect()->back();
        }

        $this->page['breadcrumb'] = self::getBreadcrumbLevelTwo($this->page['titlePage'], route($this->page['route'].'index'), 'Editar');
        return view($this->viewDir.'edit')
            ->with([
                'page'=>$this->page,
                'data'=>$data
            ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param CompanyUpdateRequest $request
     * @param Company $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, Company $company)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin', 'partner'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        $data = $request->validated();
        if (! $user->hasRole(['admin'])) {
            if ($user->id != $company->user_id) {
                toastr()->error('Usuário não vinculado');
                return redirect()->back();
            }
            $data = Arr::except($data, ['company_name', 'fantasy' ,'cnpj']);
        }

        $this->companyRepository->update($company->id, $data);

        if ($request->hasFile('file')) {
            $this->companyRepository->addCoverImage($company->id, $request->file('file'));
        }

        toastr()->success('Atualizado com sucesso!');
        return redirect()->route($this->route.'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        if (! $user->hasRole(['admin'])) {
            toastr()->error('Usuário sem permissão');
            return redirect()->back();
        }

        if  ($this->companyRepository->delete($id)) {
            toastr()->warning('Excluído com sucesso!');
            return redirect()->route($this->route.'index');
        }

        toastr()->error('Erro ao excluir!');
        return redirect()->route($this->route.'index');

    }
}
