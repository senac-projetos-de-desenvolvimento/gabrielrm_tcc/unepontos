<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProfileUpdateRequest;
use App\Models\User;
use App\Traits\BreadcrumbTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use File;

class ProfileController extends Controller
{
    use BreadcrumbTrait;

    private $route = "profile.";
    private $viewDir = 'profile.';
    private $titlePage = 'Perfil';

    public $page = [];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
        $this->middleware('permission:profile-edit', ['only' => ['edit','update']]);
        $this->page['titlePage'] = $this->titlePage;
        $this->page['route'] = $this->route;
        $this->page['viewDir'] = $this->viewDir;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('home');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit()
    {
        $data = User::where('id', Auth::id())->with('media')->first();
        $this->page['breadcrumb'] = self::getBreadcrumbLevelOne($this->page['titlePage'], route('home'), 'Dashboard');

        return view($this->viewDir.'edit')->with(['page'=>$this->page, 'data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(ProfileUpdateRequest $request)
    {
        $data = $request->all();

        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            $data = Arr::except($data,['password']);
        }

        $model = User::findOrFail(Auth::id());

        $model->update($data);

        if ($request->hasFile('file') && $request->file('file')->isValid()) {
            $model->addMediaFromRequest('file')
                ->usingFileName(bcrypt(md5(time())))
                ->toMediaCollection('profile');
        }

        toastr()->success('Atualizado com sucesso!');
        return redirect()->route('home');
    }
}
