<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Repositories\Contracts\CompanyRepositoryInterface;
use App\Repositories\Contracts\CouponRepositoryInterface;
use App\Repositories\Contracts\DonationRepositoryInterface;
use App\Repositories\Contracts\InstitutionRepositoryInterface;
use App\Services\Dashboard\Contracts\AdminPanelServiceInterface;
use App\Services\Wallet\Contracts\WalletCacheServiceInterface;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    const MAX_CARDS_LIST = 3;
    const MAX_LAST_DONATIONS = 3;
    const MAX_LAST_COUPONS = 5;

    private WalletCacheServiceInterface $walletCacheService;
    private InstitutionRepositoryInterface $instituionRepository;
    private CompanyRepositoryInterface $companyRepository;
    private DonationRepositoryInterface $donationRepository;
    private CouponRepositoryInterface $couponRepository;
    private AdminPanelServiceInterface $adminPanelService;

    private string $route = "home.";
    private string $viewDir = 'dashboard.';
    private string $titlePage = 'Painel de Controle';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        WalletCacheServiceInterface $walletCacheService,
        InstitutionRepositoryInterface $instituionRepository,
        CompanyRepositoryInterface $companyRepository,
        DonationRepositoryInterface $donationRepository,
        CouponRepositoryInterface $couponRepository,
        AdminPanelServiceInterface $adminPanelService,
    )
    {
        $this->walletCacheService = $walletCacheService;
        $this->instituionRepository = $instituionRepository;
        $this->companyRepository = $companyRepository;
        $this->donationRepository = $donationRepository;
        $this->couponRepository = $couponRepository;
        $this->adminPanelService = $adminPanelService;

        $this->middleware('auth');
        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];

        $this->middleware('permission:dashboard-list');

    }

    /**
     * Show the application admin.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('donor')) {
            return $this->dashboardDonor($user);
        }

        if ($user->hasRole('institution')) {
            return $this->dashboardInstitution($user);
        }

        if ($user->hasRole('partner')) {
            return $this->dashboardCompany($user);
        }

        if ($user->hasRole('admin')) {
            return $this->dashboardAdmin($user);
        }

        return view($this->viewDir.'index')->with(['page' => $this->page, 'data' => '']);
    }

    private function dashboardDonor(User $user)
    {
        $data['availablePoints'] = $this->walletCacheService->getSumAvailableBalanceByUser($user->id);
        $data['institutions'] = $this->instituionRepository->getAll()->shuffle()->take(self::MAX_CARDS_LIST);
        $data['companies'] = $this->companyRepository->getAll()->shuffle()->take(self::MAX_CARDS_LIST);

        return view($this->viewDir.'index-donor')->with(['page' => $this->page, 'data' => $data]);
    }

    private function dashboardInstitution(User $user)
    {
        $institution = $user->institution;
        $data['availablePoints'] = $this->donationRepository->sumAvailablePoints($institution->id);
        $data['lastDonations'] = $this->donationRepository->getLastDonationsByInstitutionId($institution->id, self::MAX_LAST_DONATIONS);
        $data['institution'] = $institution;

        $donations = $this->donationRepository->getSumPointsLastSixMonthsGroupMonthByInstitutionId($institution->id);
        foreach ($donations as $donation) {
            $data['donationsByMonth']['label'][] = $donation['date'];
            $data['donationsByMonth']['data'][] = (int) $donation['points'];
        }

        if (empty($donations)) {
            $data['donationsByMonth']['label'][] = 'Instituição sem doações nos últimos meses';
            $data['donationsByMonth']['data'][] = 0;
        }

        return view($this->viewDir.'index-institution')->with(['page' => $this->page, 'data' => $data]);
    }

    private function dashboardCompany(User $user)
    {
        $company = $user->company;

        $data['lastCoupons'] = $this->couponRepository->getLastCouponsRescuedByCompanyId($company->id, self::MAX_LAST_COUPONS);
        $data['company'] = $company;

        $coupons = $this->couponRepository->getSumPointsLastSixMonthsGroupMonth($company->id);
        foreach ($coupons as $coupon) {
            $data['couponByMonth']['label'][] = $coupon['date'];
            $data['couponByMonth']['data'][] = (int) $coupon['points'];
        }

        if (empty($coupons)) {
            $data['couponByMonth']['label'][] = 'Empresa não gerou cupons nos últimos meses';
            $data['couponByMonth']['data'][] = 0;
        }

        return view($this->viewDir.'index-company')->with(['page' => $this->page, 'data' => $data]);
    }

    private function dashboardAdmin()
    {
        $data['lastCoupons'] = $this->adminPanelService->getLastCouponsCreated();
        $data['lastDonations'] = $this->adminPanelService->getLastDonations();

        $data['widgets'] = $this->adminPanelService->getWidgets();

        $data['donationsByMonth'] = $this->adminPanelService->getSumDonationPointsLastMonthsGroupMonth();
        $data['couponsByMonth'] = $this->adminPanelService->getSumCouponsPointsLastMonthsGroupMonth();

        return view($this->viewDir.'index-admin')->with(['page' => $this->page, 'data' => $data]);
    }
}
