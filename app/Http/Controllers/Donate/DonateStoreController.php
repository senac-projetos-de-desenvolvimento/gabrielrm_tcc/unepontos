<?php

namespace App\Http\Controllers\Donate;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\DonationRequest;
use App\Repositories\Contracts\DonationRepositoryInterface;
use App\Repositories\Contracts\InstitutionRepositoryInterface;
use App\Repositories\Contracts\WalletRepositoryInterface;
use App\Services\Wallet\Contracts\WalletCacheServiceInterface;
use Illuminate\Support\Facades\Auth;

class DonateStoreController extends Controller
{
    private DonationRepositoryInterface $donationRepository;
    private InstitutionRepositoryInterface $instituionRepository;
    private WalletRepositoryInterface $walletRepository;
    private WalletCacheServiceInterface $walletCacheService;

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        DonationRepositoryInterface $donationRepository,
        WalletRepositoryInterface $walletRepository,
        InstitutionRepositoryInterface $instituionRepository,
        WalletCacheServiceInterface $walletCacheService,
    )
    {
        $this->middleware('auth');

        $this->donationRepository = $donationRepository;
        $this->walletRepository = $walletRepository;
        $this->instituionRepository = $instituionRepository;
        $this->walletCacheService = $walletCacheService;
    }

    public function __invoke(DonationRequest $request)
    {
        $data = $request->validated();

        $user = Auth::user();

        $previousRouteUrl = url()->previous();
        $previousRouteName = app('router')->getRoutes($previousRouteUrl)->match(app('request')->create($previousRouteUrl))->getName();

        $availableBalance = $this->walletCacheService->getSumAvailableBalanceByUser($user->id);
        if (empty($availableBalance) || $data['points'] > $availableBalance) {
            toastr()->warning('Saldo indisponível para concluir doação');
            return redirect()->back();
        }

        if ($this->walletRepository->userDonation($user->id, $data['points'])) {
            $data['user_id'] = $user->id;
            $this->donationRepository->createDonation($data);
            $this->walletCacheService->forgetSumAvailableBalanceByUser($user->id);

            toastr()->success('Doação realizada com sucesso');

            if ($previousRouteName == 'institutions.show') {
                return redirect()->back();
            }
            return redirect()->route('home');
        }

        toastr()->error('Tivemos desafios para concluir a operação, contate o suporte');
        return redirect()->back();
    }

}
