<?php

namespace App\Http\Controllers\Donate;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\InstitutionRepositoryInterface;

class DonateFormController extends Controller
{
    private InstitutionRepositoryInterface $instituionRepository;

    private string $route = "donate.";
    private string $viewDir = 'donate.';
    private string $titlePage = 'Fazer doação';

    public array $page = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(
        InstitutionRepositoryInterface $instituionRepository,
    )
    {
        $this->middleware('auth');
        $this->page = [
            'titlePage' => $this->titlePage,
            'route' => $this->route,
            'viewDir' => $this->viewDir,
            'breadcrumb' => [
                [
                    'title' => 'Inicio',
                    'url' => route('home')
                ],
                [
                    'title' => $this->titlePage,
                    'url' => null
                ],
            ]
        ];

        $this->instituionRepository = $instituionRepository;

    }

    public function __invoke()
    {
        $data = [];
        $data['institutions'] = $this->instituionRepository->getInstitutionsPluckIdAndName();

        return view($this->viewDir.'create')->with([
            'page'=>$this->page, 'data' => $data
        ]);
    }

}
