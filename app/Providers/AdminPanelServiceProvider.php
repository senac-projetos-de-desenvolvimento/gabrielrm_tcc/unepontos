<?php

namespace App\Providers;

use App\Services\Dashboard\AdminPanelService;
use App\Services\Dashboard\Contracts\AdminPanelServiceInterface;
use Illuminate\Support\ServiceProvider;

class AdminPanelServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            AdminPanelServiceInterface::class, AdminPanelService::class
        );
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [AdminPanelServiceInterface::class];
    }
}
