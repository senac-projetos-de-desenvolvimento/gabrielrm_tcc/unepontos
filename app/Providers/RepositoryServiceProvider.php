<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\Contracts\{CompanyRepositoryInterface,
    CouponRepositoryInterface,
    DonationRepositoryInterface,
    InstitutionRepositoryInterface,
    MenuRepositoryInterface,
    RoleRepositoryInterface,
    UserRepositoryInterface,
    WalletRepositoryInterface};

use App\Repositories\{CompanyRepository,
    CouponRepository,
    DonationRepository,
    InstitutionRepository,
    MenuRepository,
    RoleRepository,
    UserRepository,
    WalletRepository};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MenuRepositoryInterface::class, MenuRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(InstitutionRepositoryInterface::class, InstitutionRepository::class);
        $this->app->bind(CompanyRepositoryInterface::class, CompanyRepository::class);
        $this->app->bind(CouponRepositoryInterface::class, CouponRepository::class);
        $this->app->bind(WalletRepositoryInterface::class, WalletRepository::class);
        $this->app->bind(DonationRepositoryInterface::class, DonationRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
