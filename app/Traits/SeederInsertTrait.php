<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait SeederInsertTrait
{
    public function insertMenuAndMenuRole($datas, $roleId = 1)
    {
        foreach ($datas as $key => $data) {
            $menuId = DB::table('menus')->insertGetId(
                [
                    'title'     => $data['title'],
                    'icon'      => $data['icons'],
                    'url'       => $data['url'],
                    'parent_id' => $data['parent_id'],
                    'order'     => $data['order'],
                    'enabled'   => 1
                ]);
            DB::table('menu_roles')->insert([
                'menu_id' => $menuId,
                'role_id' => $roleId,
            ]);
            if (isset($data['childrens'])) {
                foreach ($data['childrens'] as $children) {
                    $childrenId = DB::table('menus')->insertGetId(
                        [
                            'title'     => $children['title'],
                            'icon'      => $children['icons'],
                            'url'       => $children['url'],
                            'parent_id' => $menuId,
                            'order'     => $children['order'],
                            'enabled'   => 1
                        ]);
                    DB::table('menu_roles')->insert([
                        'menu_id' => $childrenId,
                        'role_id' => $roleId,
                    ]);
                }
            }
        }
    }

    public function insertPermission($permissions)
    {
        $permissionsSufix = ['list', 'create', 'edit', 'delete', 'update'];
        $description = ['Listar', 'Criar', 'Editar', 'Apagar', 'Atualizar'];
        $guard = 'webmaster';

        foreach ($permissions as $permission) {
            foreach ($permissionsSufix as $key => $value) {
                $permissionId = DB::table('permissions')->insertGetId([
                    'name' => $permission['name']."-".$permissionsSufix[$key],
                    'guard_name' => $guard,
                    'description' => $description[$key],
                    'father' => $permission['father']
                ]);

                DB::table('role_has_permissions')->insert([
                    'permission_id' => $permissionId,
                    'role_id' => 1,
                ]);
            }
        }
    }
}
