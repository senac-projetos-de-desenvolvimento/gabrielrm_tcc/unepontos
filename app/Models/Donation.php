<?php


namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Donation extends Model implements HasMedia, Auditable
{
    use \OwenIt\Auditing\Auditable;
    use InteractsWithMedia;
    use SoftDeletes;

    const STATUS = [
        'rescued' => 'rescued',
        'pending' => 'pending',
        'available' => 'available',
        'expired' => 'expired',
        'suspended' => 'suspended',
        'cancelled' => 'cancelled'
    ];

    const DONATION_TYPE = 'donation';
    const RESCUE_TYPE = 'rescue';

    const MONTHS_TO_EXPIRE = 3;

    protected $fillable = [
        'id',
        'user_id',
        'institution_id',
        'points',
        'status',
        'expiration_date',
        'withdrawal_request_date',
        'rescued_by'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function institution()
    {
        return $this->belongsTo('App\Models\Institution');
    }

    public function rescued()
    {
        return $this->belongsTo('App\Models\User', 'rescued_by', 'id');
    }

    public function getLastMedia(string $collectionName = 'default', $filters = []): ?Media
    {
        $media = $this->getMedia($collectionName, $filters);

        return $media->last();
    }

    public function getLastImageUrl(string $collectionName = 'default', string $conversionName = ''): string
    {
        $media = $this->getLastMedia($collectionName);

        if (! $media) {
            return $this->getFallbackMediaUrl($collectionName) ?: '';
        }

        if ($conversionName !== '' && ! $media->hasGeneratedConversion($conversionName)) {
            return $media->getUrl();
        }

        return $media->getUrl($conversionName);
    }

    public function scopeDonationType($query)
    {
        $query->where('type', self::DONATION_TYPE);
    }

    public function scopeRescueType($query)
    {
        $query->where('type', self::RESCUE_TYPE);
    }

    public function getStatusAttribute($value)
    {
        return __('status.' . $value);
    }

    /**
     * @return string
     */
    public function rescueVoucherImage(): string
    {
        return count($this->media) ? $this->getLastImageUrl('voucher') : asset('/images/default-user.png');
    }
}
