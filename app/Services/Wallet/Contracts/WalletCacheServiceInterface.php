<?php

namespace App\Services\Wallet\Contracts;

interface WalletCacheServiceInterface
{
    /**
     * @param int $userId
     * @return int
     */
    public function getSumAvailableBalanceByUser(int $userId): int;

    /**
     * @param int $userId
     * @return bool
     */
    public function forgetSumAvailableBalanceByUser(int $userId): bool;
}
