<?php

namespace App\Services\Dashboard\Contracts;

interface AdminPanelServiceInterface
{
    /**
     * @return array
     */
    public function getWidgets(): array;

    /**
     * @return mixed
     */
    public function getLastCouponsCreated(): mixed;

    /**
     * @return mixed
     */
    public function getLastDonations(): mixed;

    /**
     * @return mixed
     */
    public function getSumDonationPointsLastMonthsGroupMonth(): mixed;

    /**
     * @return mixed
     */
    public function getSumCouponsPointsLastMonthsGroupMonth(): mixed;
}
