<?php

namespace App\Services\Dashboard;

use App\Repositories\Contracts\CouponRepositoryInterface;
use App\Repositories\Contracts\DonationRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Dashboard\Contracts\AdminPanelServiceInterface;

class AdminPanelService implements AdminPanelServiceInterface
{
    const LIMIT_RESULTS = 6;

    private DonationRepositoryInterface $donationRepository;
    private CouponRepositoryInterface $couponRepository;
    private UserRepositoryInterface $userRepository;

    public function __construct(
        DonationRepositoryInterface $donationRepository,
        CouponRepositoryInterface $couponRepository,
        UserRepositoryInterface $userRepository,
    )
    {
        $this->donationRepository = $donationRepository;
        $this->couponRepository = $couponRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @return array
     */
    public function getWidgets(): array
    {
        return [
            'coupons_created' => $this->couponRepository->countCouponsCreated(),
            'coupons_rescued' => $this->couponRepository->countCouponsRescued(),
            'donations' => $this->donationRepository->countDonationsByLastMonths(),
            'created_users' => $this->userRepository->countRegisteredUsers(),
        ];
    }

    /**
     * @return mixed
     */
    public function getLastCouponsCreated(): mixed
    {
        return $this->couponRepository->getLastCoupons(self::LIMIT_RESULTS);
    }

    /**
     * @return mixed
     */
    public function getLastDonations(): mixed
    {
        return $this->donationRepository->getLastDonations(self::LIMIT_RESULTS);
    }

    /**
     * @return mixed
     */
    public function getSumDonationPointsLastMonthsGroupMonth(): mixed
    {
        $donations = $this->donationRepository->getSumPointsLastMonthsGroupMonth();

        foreach ($donations as $donation) {
            $data['donationsByMonth']['label'][] = $donation['date'];
            $data['donationsByMonth']['data'][] = (int) $donation['points'];
        }

        if (empty($donations)) {
            $data['donationsByMonth']['label'][] = 'Não existem doações nos últimos meses';
            $data['donationsByMonth']['data'][] = 0;
        }

        return $data['donationsByMonth'];
    }

    public function getSumCouponsPointsLastMonthsGroupMonth(): mixed
    {
        $coupons = $this->couponRepository->getSumPointsLastMonthsGroupMonth();

        foreach ($coupons as $coupon) {
            $data['couponsByMonth']['label'][] = $coupon['date'];
            $data['couponsByMonth']['data'][] = (int) $coupon['points'];
        }

        if (empty($coupons)) {
            $data['couponsByMonth']['label'][] = 'Não existem cupons gerados nos últimos meses';
            $data['couponsByMonth']['data'][] = 0;
        }

        return $data['couponsByMonth'];
    }

}
