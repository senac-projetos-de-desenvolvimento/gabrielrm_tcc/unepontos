<?php

namespace App\Repositories;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Models\User;

class UserRepository extends Repository implements UserRepositoryInterface
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }


    /**
     * @return mixed
     */
    public function getAllIdDescWithMedia()
    {
        return $this->model->orderBy('id','desc')->with('media')->get();
    }

    /**
     * @param array $relations
     * @return mixed|void
     */
    public function getAllIdDescWithRelations(array $relations)
    {
        return $this->model->orderBy('id','desc')->with($relations)->get();
    }


    /**
     * @param $user_id
     * @param $role_id
     * @return mixed
     */
    public function syncRole($user_id, $role_id)
    {
        return $this->model->find($user_id)->roles()->sync($role_id);
    }

    /**
     * @param $user_id
     * @param $file
     * @return mixed
     */
    public function addImageProfile($user_id,$file)
    {
        return $this->model->find($user_id)
            ->addMedia($file)
//            ->addMediaFromRequest('file')
            ->usingFileName(bcrypt(md5(time())))
            ->toMediaCollection('profile');
    }

    /**
     * @return mixed
     */
    public function getAllResponsibleOptions()
    {
        return $this->model->orderBy('name')->pluck('name','id');
    }

    /**
     * @return int
     */
    public function countRegisteredUsers(): int
    {
        return $this->model->count();
    }

}
