<?php

namespace App\Repositories\Contracts;


interface RoleRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAllByName();
}
