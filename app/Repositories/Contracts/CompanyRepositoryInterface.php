<?php

namespace App\Repositories\Contracts;


interface CompanyRepositoryInterface
{
    public function getCompaniesPluckIdAndName();

    /**
     * @param int $id
     * @param $file
     * @return mixed
     */
    public function addCoverImage(int $id, $file);
}
