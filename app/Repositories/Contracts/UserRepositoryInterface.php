<?php

namespace App\Repositories\Contracts;


interface UserRepositoryInterface
{
    /**
     * @return mixed
     */
    public function getAllIdDescWithMedia();

    /**
     * @param array $relations
     * @return mixed
     */
    public function getAllIdDescWithRelations(array $relations);

    /**
     * @param $user_id
     * @param $role_id
     * @return mixed
     */
    public function syncRole($user_id, $role_id);

    /**
     * @param $user_id
     * @param $file
     * @return mixed
     */
    public function addImageProfile($user_id, $file);

    /**
     * @return mixed
     */
    public function getAllResponsibleOptions();

    /**
     * @return int
     */
    public function countRegisteredUsers(): int;
}
